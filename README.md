$${\color{red} \text{\Huge This documentation page is outdated!}}$$

$${\color{red} \text{\Huge Please go to the new VMM3a/SRS documentation on}}$$

$${\color{red} \text{\Huge vmm-srs.docs.cern.ch}}$$


VMM3a/SRS software, firmware and hardware guidance
==================================================


[*Lucian Scharenberg,*](mailto:lucian.scharenberg@cern.ch)
Dorothea Pfeiffer, Hans Muller <br>
with contributions from Karl Flöthner, Gerardo Romero, Tassos Belias,
Daichi Nagasaki and Marco Sessa

*04 February 2022*

## Where to find what?
Before going through the introduction, here some important links on where
to find various things related to the VMM3a front-end of the SRS.
The links do **not** replace to search inside this document for answers!

* [**OPERATING:** tutorial video on how to operate VMM3a/SRS](https://indico.cern.ch/event/1071632/contributions/4615369/attachments/2345888/4000941/VMM-SRS-SoftwareInstallation-Scharenberg.mp4)
* [**HARDWARE INFO:** Google Drive from Hans Muller for all hardware components](https://drive.google.com/drive/folders/1h5KLMAa7-bxbPoisnip5Adb4mYc2YMdq)
* [**USER GUIDE:** this document](https://gitlab.cern.ch/rd51-slow-control/vmm-doc)
* [**COMMUNITY FORUM:** Discord server](https://discord.gg/jsT9HgD)
<br>


* [**CONTROL SOFTWARE:** VMM3a/SRS slow control software from RD51](https://gitlab.cern.ch/rd51-slow-control/vmm-doc)
* [**ONLINE MONITORING:** acquisition and monitoring software from ESS](https://github.com/ess-dmsc/essdaq)
* [**DATA ACQUISITION:** Tcpdump for the data taking](https://www.tcpdump.org/)
* [**DATA RECONSTRUCTION:** vmm-sdat for the cluster reconstruction](https://github.com/ess-dmsc/vmm-sdat)
<br>


* [**WG5.1 MEETINGS:** meetings for everybody who is using/wants to use VMM3a/SRS](https://indico.cern.ch/category/11660/)

## Introduction

With this document is a little guidance through the first steps of using
VMM3a/SRS should be provided, allowing to successfully operate the system.
It does not provide any information on which electronics settings should be used
for which experiment and physics application.
Furthermore it is assumed, that the reader is aware of the principle structure
of RD51’s Scalable Readout System (SRS).
In case there are doubts, it is referred e.g. to
https://doi.org/10.1088/1748-0221/8/03/C03015

One comment to the reader before the start: please keep in mind that VMM3a/SRS
is a powerful system but also a system which is further developed and improved.
So not everything may run as smooth as you may think/hope/wish.
Therefore, the contribution of the reader to the development of the system and to
help to create a mature readout system for particle detectors is essential.

The same applies to this document.
It is _always_ work in progress and changes accordingly to the developments on the system.

For changes and suggestions to improve this document it is recommended to use
the standard `git` tools, so pull requests (GitHub) or merge requests (GitLab).
_Or just create an issue on the CERN GitLab webpage of this document_.

As we are here working with GitLab and in case you might not know how to use
git and how to do merge requests, please find some information under the
following links (last accessed 27 April 2021):
* https://information-technology.web.cern.ch/services/git-service
* https://git-scm.com/doc
* https://docs.gitlab.com/ee/gitlab-basics/
* https://docs.gitlab.com/ee/user/project/merge_requests/
* https://docs.gitlab.com/ee/user/project/merge_requests/getting_started.html
* https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html


## Tutorial video
Everybody who wants to use VMM and SRS should read thoroughly through  this guide, and check
if their question/problem was already answered/described here.

It is however important to mention, that a video is available, where the components
and the principle operating scheme of the system are described.
So also please check out the tutorial video, it may answer your question already:
https://indico.cern.ch/event/1071632/contributions/4615369/


## Requirements (hardware, firmware and software)

#### Minimal system
The hardware components for a minimal system of VMM3a/SRS are (they can be
all purchased via SRS technology):
* RD51 VMM hybrids
* HDMI cables
* Ground return cables
* Digital VMM adapter (DVMM) card
* Front-End Concentrator (FEC) card
* SRS Power Crate 2k
* Ethernet cable
* DAQ computer

#### DAQ computer (minimal)
In principle each normal PC will do, which has Ubuntu 18.04 LTS running

#### DAQ computer (recommended)
* Ubuntu 18.04 LTS (freshly installed without any additions)
* 32 GB of RAM
* ~ 8 core CPU
* 2 TB SSD
* 10 Gbps network card with support for Jumbo-frames (Intel X710T2L was tested and works)
* 10 Gbps network switch (Netgear GS110MX was tested and works)

#### DAQ computer (questions)
Please have a look at the troubleshooting, if you have questions on the computing hardware.

#### IMPORTANT Note on software
The combination of VMM3a and SRS is a new system with its own pieces of software.
The VMM3a is also a new chip, which works differently to previous ASICs that have been implemented into the SRS, i.e. the APV25.
This means that the software for VMM3a/SRS was freshly developed. It also means that no old software for APV25/SRS was adapted towards VMM3a/SRS.
It means even further, that the user should (for the beginning) ignore all their knowledge
about old or other DAQ systems and not compare it with these previous systems.

#### Software (required)
* RD51 VMM slow control software (can be found here:
[Slow Control](https://gitlab.cern.ch/rd51-slow-control/vmmsc)).
The _**main**_ branch is recommended.
* To use the slow control, Qt has to be installed on the computer. **Version 5.12.9** was found to be working well.

#### Software (recommended)
Additionally the following software for data
taking, data analysis and online monitoring _can_ be used:
* Wireshark
* DAQ and online monitoring software from the European Spallation Source
(ESS): [ESS DAQ](https://github.com/ess-dmsc/essdaq).
It consists out of the _Event Formation Unit_ (EFU) for the online cluster
reconstruction and _DAQuiri_ for the graphical representation of these clusters.
* Tcpdump to record at high data rates:
[Tcpdump](https://github.com/the-tcpdump-group/tcpdump)
* vmm-sdat for the data analysis:
[vmm-sdat](https://github.com/ess-dmsc/vmm-sdat).

It is also recommended to use gcc/g++ in version 9 and CMake in version 3.21.X.
For the installation of gcc/g++ in the relevant version see the slow control repository
[Slow Control](https://gitlab.cern.ch/rd51-slow-control/vmmsc), as this should be the
starting point for any VMM3a/SRS related software installation.

#### Software (further information)
In addition to this, it should be noted, that the VMM slow control (will be
shown later on) has an integrated VMM test module.
The data from the VMM tests can be stored in a data base, of which the data
base browser can be found here:
https://github.com/FinnJaekel/VMM-Database-Browser


## Setting up the system
For setting up the system, please see the following links and sub-directories of this guide.
* [Software installation](https://gitlab.cern.ch/rd51-slow-control/vmm-doc/-/tree/master/software-installation/)
* [Firmware](https://gitlab.cern.ch/rd51-slow-control/vmm-doc/-/tree/master/firmware)
* [System operation](https://gitlab.cern.ch/rd51-slow-control/vmm-doc/-/tree/master/operating/)
* [Troubleshooting](https://gitlab.cern.ch/rd51-slow-control/vmm-doc/-/tree/master/troubleshooting/)


## Operating the system
To see, how to operate the system, please have a look at the tutorial
video, mentioned above, as well as the
[system operation](https://gitlab.cern.ch/rd51-slow-control/vmm-doc/-/tree/master/operating/)
section of this guide.
Within this section, please pay particular attention to the
[*VMM and SRS - a guide for dummies*](https://gitlab.cern.ch/rd51-slow-control/vmm-doc/-/tree/master/operating/vmm-and-srs-infn-rome.pdf).

## Contacts

In case of persistent problems, join the discussion in one of the corresponding
channels of Discord and post it there.
Maybe your question was also already asked there, so please check this
before posting: https://discord.gg/jsT9HgD

If you want to contact one of the developers directly, please check out the
README files on the GitHub or GitLab pages of the software/firmware projects.
Usually the contact details are mentioned there.

If you have a very specific problem, which could not be solved with this
document or the above hints you can contact the following people:
* Electronics in general, hardware: hans.muller@cern.ch, kaminski@physik.uni-bonn.de
* General questions, system operation: lucian.scharenberg@cern.ch, dorothea.pfeiffer@cern.ch, michael.lupberger@cern.ch
* Software: dorothea.pfeiffer@cern.ch, michael.lupberger@cern.ch, lucian.scharenberg@cern.ch
* Firmware: michael.lupberger@cern.ch, dorothea.pfeiffer@cern.ch
* Virtual machine: francisco.garcia@cern.ch
* Discord: caiazza@uni-mainz.de, michael.lupberger@cern.ch
* SRS technology: alex.rusu@srstechnology.ch
