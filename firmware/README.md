VMM3a/SRS firmware
==================

## General info
As the reader may know, the SRS needs a specific FPGA firmware for each
front-end ASIC.
In case of VMM3a/SRS the system has *two* FPGAs (one on the FEC and one on
the hybrid) which require individual firmware versions.

For (re-)programming the FPGAs (and to generate the `.mcs` files for a
permanent flashing), see
[Firmware Upgrade Tutorial](https://espace.cern.ch/rd51-wg5/srs/Documentation/FEC_3_6_Firmware_Upgrade_Tutorial%20(3).pdf)
and
[VMM hybrid configuration](https://drive.google.com/file/d/1VZJY-wE49nfGjuopBCZCbm7NnMJyR7mb/view?usp=sharing)

The programmable devices (flash memory for the FPGAs) are (here are only the product numbers mentioned):
* FEC v6 (Virtex 6) `XC6VLX130T-1FFG784C`  
  pre 2019 -> 64 Mbit `S25FL064P0XNFI001`  
  2019+    -> 128 Mbit `MT25QL128ABA`
* RD51 VMM hybrid (Spartan 6) `XC6SLX16-2CSG225C`  
  upcoming productions 2021/22 (Spartan 7, `XC7S25-1CSGA324C`)  
  Boot Flash (until 2020)	-> 16 Mbit `AT45DB161E`  
  Boot Flash (from 2021) 	-> 32 Mbit `AT45DB321E-SHF2B-T`  
  Auxiliary Flash/Serial Nr: 4 kbit `AT24CS04-STUM`


**IMPORTANT NOTE:** the production date for the FEC (pre 2019 or 2019+) is NOT the text written on the PCB.
This is the design date. The production date is written on a little white sticker on the PCB:
![](./image_fec-version-sticker.png)
In this example, the FEC was produced in October 2017 and not in May 2014!

FEC MAC address as from 2015 (MAC address label and EEPROM):
`00.50.C2`  
Default FEC IP: `10.0.0.2`  
Note: old MAC addresses (type `08-00-30-F2-01-00`) must be reprogrammed
Serial Numbers: SAMWAY, see FEC EEPROM programming instructions


## Bit files to flash the FPGAs with
The `.bit` files to generate the `.mcs` can be found either in the **main** branch of the
GitLab repository of the slow control software
* Hybrid: [`vmm3h_1_020920_20201118.bit`](https://gitlab.cern.ch/rd51-slow-control/vmmsc/-/blob/main/vmm3h_1_020920_20201118.bit)
* FEC: [`fecv6_vmm3_top_21031200.bit`](https://gitlab.cern.ch/rd51-slow-control/vmmsc/-/blob/main/fecv6_vmm3_top_21031200.bit)

or alternatively in this folder.


## Settings for flashing the FPGAs
The following settings should be selected:
* SPI flash
* FECs produced before 09/2019 (see the sticker on the FEC stating the data),
select `S25FL064P` in Xilinx iMPACT
* FECs produced after 09/2019, select `N25Q128 1.8/3.3 V`

Although the hybrids should have a working firmware flashed, the reader may want
to upload a more recent version.
If so, a JTAG adapter is needed.
This can be purchased via SRS technology.

From previous experience it should be noted that the software used to program
the FPGAs (Xilinx iMPACT) seems work best when directly installed on a Windows
computer (not a virtual machine, not a different operating system).
If the reader decides to use nonetheless something else then the proposed
configuration (directly installed on Windows) it is not guaranteed that the
flashing will work.
