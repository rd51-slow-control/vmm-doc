Troubleshooting
===============

As first rule for the troubleshooting, please follow "Muller's Law":<br>
§1: if a bug is declared as HW look for a bug in FW or SW<br>
§2: if FW or SW declares to be bug free go to §1<br>

Or in other words: the hardware that you have should be fine. If you encounter some strange
behaviour, first check that the software works correctly, that you have the correct firmware
loaded, and that fundamental hardware "standards" (e.g. good grounding, which is super important for VMM3a/SRS)
are fulfilled.

## Software installation
* When you try to start the slow control and have an error message similar to this one
  ```
  qt.qpa.plugin: Could not find the Qt platform plugin "xcb" in ""
  This application failed to start because no Qt platform plugin could be initialized. Reinstalling the application may fix this problem
  ```
  try the following: add
  ```
  export QT_PLUGIN_PATH=/home/USERNAME/Qt5.12.9/5.12.9/gcc_64/plugins
  ```
  to your ``.bashrc`` and reinstall the Slow Control via
  ``make clean``, ``qmake vmmdcs.pro`` and ``make`` in the build-directory
  of the Slow Control folder.

* Installation of the Event Formation Unit (EFU):
  If executing ``cmake ..`` takes very long, without any output, do not wory. This can happen. It may last 10 minutes or even longer.
  It can take veryyyyy long!

* Installation of the Event Formation Unit (EFU).
  If you have an error message similar to this one:
  ```
  ERROR: The remote at 'https://bincrafters.jfrog.io/artifactory/api/conan/public-conan' only works with revisions enabled.
  Set CONAN_REVISIONS_ENABLED=1 or set 'general.revisions_enabled = 1' at the 'conan.conf'. [Remote: bincrafters]
  ```
  Please add ``revisions_enabled = 1`` underneath of ``[general]`` in
  ``conan.conf`` in the ``.conan`` directory inside of your home directory.

* Installation of the Event Formation Unit (EFU).
  If you have an error message similar to this one:
  ```
  ERROR: 404: Not Found. [Remote: conancenter]
  The 'benchmark/1.5.0' package has 'exports_sources' but sources not found in local cache.
  Probably it was installed from a remote that is no longer available.
  CMake Error at cmake/modules/conan.cmake:371 (message):
  Conan install failed='1'
  Call Stack (most recent call first):
  cmake/modules/conan.cmake:451 (conan_cmake_install)
  CMakeLists.txt:37 (conan_cmake_run)
  ```
  Please do the following:
  go to the ``.conan/data`` directory inside of your home directory.
  Execute
  ```
  conan remove benchmark
  conan install benchmark/1.5.0@
  ```
  This error can occur with any conan package. It can be solved in the
  fashion. For example
  ```
  conan remove fmt
  conan install fmt/6.2.0@
  ```

* In case you get an error while installing a conan package, which looks like this
  ```
  dateuser@MiniHP-VMM:~/.conan/data$ conan install benchmark/1.5.0@
  Configuration:
  [settings]
  arch=x86_64
  arch_build=x86_64
  build_type=Release
  compiler=gcc
  compiler.libcxx=libstdc++11
  compiler.version=9.4
  os=Linux
  os_build=Linux
  [options]
  [build_requires]
  [env]

  benchmark/1.5.0: Not found in local cache, looking in remotes...
  benchmark/1.5.0: Trying with 'conancenter'...
  Downloading conanmanifest.txt completed [0.17k]                                          
  Downloading conanfile.py completed [3.69k]                                               
  Downloading conan_export.tgz completed [0.24k]                                           
  Decompressing conan_export.tgz completed [0.00k]                                         
  benchmark/1.5.0: Downloaded recipe revision 8f9fda5c5a906b4f3f2f10ca68922ecf
  Installing package: benchmark/1.5.0
  Requirements
      benchmark/1.5.0 from 'conancenter' - Downloaded
  Packages
      benchmark/1.5.0:2ce7dd302c24e6ab9c4dfc02348bacb2759d092a - Missing

  Installing (downloading, building) binaries...
  ERROR: Missing binary: benchmark/1.5.0:2ce7dd302c24e6ab9c4dfc02348bacb2759d092a

  benchmark/1.5.0: WARN: Can't find a 'benchmark/1.5.0' package for the specified settings, options and dependencies:
  - Settings: arch=x86_64, build_type=Release, compiler=gcc, compiler.libcxx=libstdc++11, compiler.version=9.4, os=Linux
  - Options: enable_exceptions=True, enable_lto=False, fPIC=True, shared=False
  - Dependencies:
  - Requirements:
  - Package ID: 2ce7dd302c24e6ab9c4dfc02348bacb2759d092a

  ERROR: Missing prebuilt package for 'benchmark/1.5.0'
  Try to build from sources with '--build=benchmark'
  Use 'conan search <reference> --table table.html'
  Or read 'http://docs.conan.io/en/latest/faq/troubleshooting.html#error-missing-prebuilt-package'
  ```

  or like this

  ```
  dateuser@MiniHP-VMM:~/.conan/data$ conan install fmt/6.2.0@
  Configuration:
  [settings]
  arch=x86_64
  arch_build=x86_64
  build_type=Release
  compiler=gcc
  compiler.libcxx=libstdc++11
  compiler.version=9.4
  os=Linux
  os_build=Linux
  [options]
  [build_requires]
  [env]

  fmt/6.2.0: Not found in local cache, looking in remotes...
  fmt/6.2.0: Trying with 'conancenter'...
  Downloading conanmanifest.txt completed [0.42k]                                          
  Downloading conanfile.py completed [4.33k]                                               
  Downloading conan_export.tgz completed [0.24k]                                           
  Decompressing conan_export.tgz completed [0.00k]                                         
  fmt/6.2.0: Downloaded recipe revision 6e58c9a85c09b9b672d5adbc4734125d
  Installing package: fmt/6.2.0
  Requirements
      fmt/6.2.0 from 'conancenter' - Downloaded
  Packages
      fmt/6.2.0:2558d3f530bd573703a3a178886bc65bf0814523 - Missing

  Installing (downloading, building) binaries...
  ERROR: Missing binary: fmt/6.2.0:2558d3f530bd573703a3a178886bc65bf0814523

  fmt/6.2.0: WARN: Can't find a 'fmt/6.2.0' package for the specified settings, options and dependencies:
  - Settings: arch=x86_64, build_type=Release, compiler=gcc, compiler.libcxx=libstdc++11, compiler.version=9.4, os=Linux
  - Options: fPIC=True, header_only=False, shared=False, with_fmt_alias=False
  - Dependencies:
  - Requirements:
  - Package ID: 2558d3f530bd573703a3a178886bc65bf0814523

  ERROR: Missing prebuilt package for 'fmt/6.2.0'
  Try to build from sources with '--build=fmt'
  Use 'conan search <reference> --table table.html'
  Or read 'http://docs.conan.io/en/latest/faq/troubleshooting.html#error-missing-prebuilt-package'
  ```

  do the following: go to the ``.conan/data`` directory and try to install the package like this

  ```
  conan install benchmark/1.5.0@ --build
  conan install fmt/6.2.0@ --build
  ```

* You might get a conan-related error, which states that the compiler
  and the compiler version are not stated. To solve this, go to the
  ``settings.yml`` file in the ``.conan`` directory and add
  ```
  compiler=gcc
  compiler.version=9.X
  ```
  with ``X`` being 3 or 4 (check via ``g++ --version``).

* If you get an error like this
  ```
  CMake Deprecation Warning at CMakeLists.txt:1 (cmake_minimum_required):
  Compatibility with CMake < 2.8.12 will be removed from a future version of
  CMake.

  Update the VERSION argument <min> value or use a ...<max> suffix to tell
  CMake that the project does not need compatibility with older versions.

  -- Conan: Automatic detection of conan settings from cmake
  -- Conan: Settings= -pr;default
  -- Conan executing: conan install /home/dateuser/VMM/essdaq/efu/event-formation-unit/conanfile.txt -pr default -g=cmake --build=outdated --no-imports
  -- ERROR: compiler not defined for compiler.libcxx
  Please define compiler value first too

  CMake Error at cmake/modules/conan.cmake:371 (message):
    Conan install failed='1'
  Call Stack (most recent call first):
    cmake/modules/conan.cmake:451 (conan_cmake_install)
    CMakeLists.txt:37 (conan_cmake_run)

  -- Configuring incomplete, errors occurred!
  See also "/home/dateuser/VMM/essdaq/efu/event-formation-unit/build/CMakeFiles/CMakeOutput.log".
  ```
  add
  ```
  compiler=gcc
  compiler.version=9.4
  ```
  in ``~/.conan/profiles/default`` BEFORE ``compiler.libcxx=libstdc++11``
  and reboot!

* If you get an error like this
  ```
  CMake Deprecation Warning at CMakeLists.txt:1 (cmake_minimum_required):
  Compatibility with CMake < 2.8.12 will be removed from a future version of
  CMake.

  Update the VERSION argument <min> value or use a ...<max> suffix to tell
  CMake that the project does not need compatibility with older versions.

  -- The C compiler identification is GNU 9.4.0
  -- The CXX compiler identification is GNU 9.4.0
  -- Detecting C compiler ABI info
  -- Detecting C compiler ABI info - done
  -- Check for working C compiler: /usr/bin/cc - skipped
  -- Detecting C compile features
  -- Detecting C compile features - done
  -- Detecting CXX compiler ABI info
  -- Detecting CXX compiler ABI info - done
  -- Check for working CXX compiler: /usr/bin/g++-9 - skipped
  -- Detecting CXX compile features
  -- Detecting CXX compile features - done
  -- Found Git: /usr/bin/git (found version "2.17.1")
  -- Conan: Automatic detection of conan settings from cmake
  -- Conan: Settings= -pr;default
  -- Conan executing: conan install /home/dateuser/VMM/essdaq/efu/event-formation-unit/conanfile.txt -pr default -g=cmake --build=outdated --no-imports
  -- ERROR: Invalid setting '9.4' is not a valid 'settings.compiler.version' value.
  Possible values are ['4.1', '4.4', '4.5', '4.6', '4.7', '4.8', '4.9', '5', '5.1', '5.2', '5.3', '5.4', '5.5', '6', '6.1', '6.2', '6.3', '6.4', '6.5', '7', '7.1', '7.2', '7.3', '7.4', '7.5', '8', '8.1', '8.2', '8.3', '8.4', '9', '9.1', '9.2', '9.3', '10', '10.1']
  Read "http://docs.conan.io/en/latest/faq/troubleshooting.html#error-invalid-setting"

  CMake Error at cmake/modules/conan.cmake:371 (message):
    Conan install failed='1'
  Call Stack (most recent call first):
    cmake/modules/conan.cmake:451 (conan_cmake_install)
    CMakeLists.txt:37 (conan_cmake_run)

  -- Configuring incomplete, errors occurred!
  See also "/home/dateuser/VMM/essdaq/efu/event-formation-unit/build/CMakeFiles/CMakeOutput.log".
  ```
  edit ``.conan/settings.yml`` and add ``9.4``, to the gcc versions:
  ```
  compiler:
    sun-cc:
        version: ["5.10", "5.11", "5.12", "5.13", "5.14", "5.15"]
        threads: [None, posix]
        libcxx: [libCstd, libstdcxx, libstlport, libstdc++]
    gcc: &gcc
        version: ["4.1", "4.4", "4.5", "4.6", "4.7", "4.8", "4.9",
                  "5", "5.1", "5.2", "5.3", "5.4", "5.5",
                  "6", "6.1", "6.2", "6.3", "6.4", "6.5",
                  "7", "7.1", "7.2", "7.3", "7.4", "7.5",
                  "8", "8.1", "8.2", "8.3", "8.4",
                  "9", "9.1", "9.2", "9.3", "9.4",
                  "10", "10.1"]
  ```

* Building the EFU. Once ``cmake ..`` of the EFU was successful and
  you try to build it via ``make`` and get an error containing something like
  this
  ```
  rdkafka_msgset_writer.c:(.text+0x4fb1): undefined reference to `mtx_lock'
  rdkafka_msgset_writer.c:(.text+0x4ffb): undefined reference to `mtx_unlock'
  rdkafka_msgset_writer.c:(.text+0x511a): undefined reference to `mtx_lock'
  rdkafka_msgset_writer.c:(.text+0x516b): undefined reference to `mtx_unlock'
  ```
  do the following:
  ```
  sudo apt install librdkafka
  sudo apt install librdkafka-dev
  cd ~/.conan/data
  conan install librdkafka/1.6.0@ --build
  ```
  Then, go back to the EFU-build-directory, execute ``cmake ..`` again,
  and try to build the EFU again via ``make -j4``.

* If you run into the following ``QMutex``-related error,
  while building DAQuiri
  ```
  Error:  In file included from /home/meddaq/essdaq/daquiri/daquiri/source/daqlite/WorkerThread.cpp:8:
  /home/meddaq/essdaq/daquiri/daquiri/source/daqlite/WorkerThread.h:36:3: error: ‘QMutex’ does not name a type
    36 |   QMutex mutex;
        |   ^~~~~~
  /home/meddaq/essdaq/daquiri/daquiri/source/daqlite/WorkerThread.cpp: In member function ‘virtual void WorkerThread::run()’:
  /home/meddaq/essdaq/daquiri/daquiri/source/daqlite/WorkerThread.cpp:23:25: warning: comparison of integer expressions of different signedness: ‘std::chrono::duration<long int, std::ratio<1, 1000000000> >::rep’ {aka ‘long int’} and ‘long long unsigned int’ [-Wsign-compare]
    23 |     if (elapsed.count() >= 1000000000ULL) {
        |         ~~~~~~~~~~~~~~~~^~~~~~~~~~~~~~~~
  /home/meddaq/essdaq/daquiri/daquiri/source/daqlite/WorkerThread.cpp:24:7: error: ‘mutex’ was not declared in this scope; did you mean ‘std::mutex’?
    24 |       mutex.lock();
        |       ^~~~~
        |       std::mutex
  In file included from /usr/include/c++/9/mutex:43,
                  from /usr/include/c++/9/future:38,
                  from /home/meddaq/Qt-5.12.9/5.12.9/gcc_64/include/QtCore/qthread.h:50,
                  from /home/meddaq/Qt-5.12.9/5.12.9/gcc_64/include/QtCore/QThread:1,
                  from /home/meddaq/essdaq/daquiri/daquiri/source/daqlite/WorkerThread.h:16,
                  from /home/meddaq/essdaq/daquiri/daquiri/source/daqlite/WorkerThread.cpp:8:
  /usr/include/c++/9/bits/std_mutex.h:83:9: note: ‘std::mutex’ declared here
    83 |   class mutex : private __mutex_base
        |         ^~~~~
  source/daqlite/CMakeFiles/daqlite.dir/build.make:173: recipe for target 'source/daqlite/CMakeFiles/daqlite.dir/WorkerThread.cpp.o' failed
  make[2]: *** [source/daqlite/CMakeFiles/daqlite.dir/WorkerThread.cpp.o] Error 1
  CMakeFiles/Makefile2:795: recipe for target 'source/daqlite/CMakeFiles/daqlite.dir/all' failed
  make[1]: *** [source/daqlite/CMakeFiles/daqlite.dir/all] Error 2
  Makefile:90: recipe for target 'all' failed
  make: *** [all] Error 2
  ```
  just add ``#include <QMutex>`` to
  ``/home/USERNAME/essdaq/daquiri/daquiri/source/daqlite/WorkerThread.h``

## Software operation
* It is highly recommended to install the ESSDAQ software in the user home directory. Not in a specific subfolder where all VMM3a/SRS related  software
  is placed.
  If the user however decides to still create a different path, the following error might occur:
  ```
  START GDGEM
  Gd-GEM - applying local configuration
  Ubuntu: net.core.netdev_max_backlog
  Checking kernel buffer sizes
  net.core.rmem_max = 12582912
  net.core.rmem_max = 12582912
  net.core.wmem_max = 12582912
  net.core.netdev_max_backlog = 5000
  HW check PASSED
  Network check started
  Network check PASSED
  start Daquiri acquisition
  START EFU
  Gd-GEM - applying local configuration
  Using default essdaq location
  Error: No config file: /home/dateuser/essdaq/detectors/gdgem/config.ini
  exiting ...
  ```

  In case this error occurs, please adjust the CONFIG_FILE path in ``essdaq/efu/efu_start.sh``:

  <pre><code>
  #!/bin/bash

  echo "START EFU"

  source ../../config/scripts/base.sh

  #
  # #
  #

  # This is no good, cannot differentiate other processes that may have "efu" somewhere in the string
  # don't start if EFU is running
  #ps aux | grep -v grep | grep efu && errexit "EFU is already running on this machine"

  UDPARG=""
  if [[ $EFU_UDP != "" ]]; then
    UDPARG="-p $EFU_UDP"
  fi

  if [[ $ESSDAQROOT != "" ]]; then
    echo "Using custom essdaq location: $ESSDAQROOT"
    CONFIG_FILE=$ESSDAQROOT/essdaq/detectors/$DETECTOR/config.ini
  else
    echo "Using default essdaq location"
    CONFIG_FILE=$HOME/ <b> + + + PATH TO ESSDAQ + +  </b> /essdaq/detectors/$DETECTOR/config.ini
  fi

  NOHWARG=""
  if [[ $NOHW != "" ]]; then
    NOHWARG="--nohwcheck"
  fi

  test -f $CONFIG_FILE || errexit "No config file: $CONFIG_FILE"

  echo "Extra EFU args: $@"

  pushd ../../efu/event-formation-unit/build &> /dev/null || errexit "directory ./event-formation-unit/build does not exist"
    if [ -z "$DEBUG" ]; then
      ./bin/efu --read_config $CONFIG_FILE $UDPARG $NOHWARG -b $KAFKA_IP:9092 -g $GRAFANA_IP --log_file ../../logfile.txt $@ 2>&1 > /dev/null &
    else
      ./bin/efu --read_config $CONFIG_FILE $UDPARG $NOHWARG -b $KAFKA_IP:9092 -g $GRAFANA_IP $@
    fi
  popd
  </code></pre>

* It might happen that DAQuiri does not display anything despite the correct settings of all components.
  It could be that there is some problem with the NMX profile files.
  So rename the existing profile files (``default_consumers.daq`` and ``profile.set`` in ``essdaq/daquiri/profiles/nmx``) and try to operate the
  system with the files, which are provided in this folder here: [``default_consumers.daq``](https://gitlab.cern.ch/rd51-slow-control/vmm-doc/-/blob/master/troubleshooting/default_consumers.daq) and [``profile.set``](https://gitlab.cern.ch/rd51-slow-control/vmm-doc/-/blob/master/troubleshooting/profile.set).
  It might be useful to reboot the computer before operating with the new profile files.

* When you start Kafka or check if Kafka is running (``essdaq/kafka/start_kafka.sh`` or ``essdaq/kafka/check_kafka.sh``), this error might occur:
  ```
  ~/essdaq/kafka ~/essdaq/kafka
  ./check_kafka.sh: line 7: ../config/system.sh: No such file or directory
  Checking Kafka at ip address
  usage: check_kafka.py [-h] bootstrap_servers
  check_kafka.py: error: the following arguments are required: bootstrap_servers
  checking for local kafka instance
  ./check_kafka.sh: line 14: netstat: command not found
  No local Kafka found
  ```
  This means that the ``system.sh`` file has not been created as described in the software configuration section of this document.

* If you get an error related to Kafka like this:
  ```
  Checking Kafka at ip address 127.0.0.1
  Brokers found
  checking for local kafka instance
  ./check_kafka.sh: line 14: netstat: command not found
  No local Kafka found
  ```
  Please install the net-tools: ``sudo apt install net-tools``

* ``kafka_check.sh`` might say that ``No local Kafka found``, after execute ``kafka_start.sh`` without error message.
  Wait 5 seconds and check again. If Kafka is still not found, try ``kafka_start.sh`` again, wait and check again.
  Normally, Kafka should run now.
  If Kafka still is not running, do the following: go to ``essdaq/kafka/kafka/logs/`` and remove the content of the entire folder.
  Then try again to start Kafka and check again.

* When starting the EFU with the ``./start.sh`` script and an error like this occurs
  ```
  Gd-GEM - applying local configuration
  Ubuntu: net.core.netdev_max_backlog
  Checking kernel buffer sizes
  net.core.rmem_max = 12582912
  net.core.rmem_max = 12582912
  net.core.wmem_max = 12582912
  net.core.netdev_max_backlog = 5000
  HW check PASSED
  Network check started
  Network check PASSED
  start Daquiri acquisition
  START EFU
  Gd-GEM - applying local configuration
  Using default essdaq location
  Extra EFU args: --file /home/dateuser/VMM/essdaq/detectors/gdgem/RHUM_config.json --calibration /home/dateuser/VMM/essdaq/detectors/gdgem/RHUM_calib.json
  ~/VMM/essdaq/detectors/gdgem
  dateuser@MiniHP-VMM:~/VMM/essdaq/detectors/gdgem$ ../../efu/efu_start.sh: line 39: ./bin/efu: No such file or directory
  ```
  make sure that the EFU is actually compiled. This means, check that you followed all the instructions under point 9
  of the installation procedure.
  You can check, if the EFU is installed if the file ``essdaq/efu/event-formation-unit/build/bin/efu`` exists.

* When starting the EFU, an error message like this shows up:
  ```
  START GDGEM
  Gd-GEM - applying local configuration
  Ubuntu: net.core.netdev.max_backlog
  Error: ethif [enp0s31f6] - MTU is not 9000 bytes
  exiting ...
  ```
  Ensure that all network interfaces are set to an MTU of 9000 (e.g. ``sudo ifconfig enp0s31f6 mtu 9000``)

* When starting the EFU and an error message like this shows up:
  ```
  dateuser@MiniHP-VMM:~/VMM/essdaq/detectors/gdgem$ ./start.sh
  START GDGEM
  Gd-GEM - applying local configuration
  Ubuntu: net.core.netdev_max_backlog
  Checking kernel buffer sizes
  net.core.rmem_max = 12582912
  net.core.rmem_max = 12582912
  net.core.wmem_max = 12582912
  net.core.netdev_max_backlog = 5000
  HW check PASSED
  Network check started
  Network check PASSED
  start Daquiri acquisition
  START EFU
  Gd-GEM - applying local configuration
  Using default essdaq location
  Extra EFU args: --file /home/dateuser/VMM/essdaq/detectors/gdgem/RHUM_config.json --calibration /home/dateuser/VMM/essdaq/detectors/gdgem/RHUM_calib.json
  ~/VMM/essdaq/detectors/gdgem
  dateuser@MiniHP-VMM:~/VMM/essdaq/detectors/gdgem$ terminate called after throwing an instance of 'std::runtime_error'
  what():  NMXConfig error - Invalid JSON file.
  ```
  please check the JSON files (calibration and configuration)

* When starting the EFU and an error message related to something like ``max_backlog ERROR`` shows up, do the following:
  open ``essdaq/detectors/gdgem/hwcheck.sh`` and replace <br>
  ``sysctl -a 2>/dev/null | grep net.core.netdev.max_backlog | grep $backlogsize || errexit "max_backlog size incorrect"``  <br>
  with  <br>
  ``sysctl -a 2>/dev/null | grep net.core.netdev_max_backlog | grep $backlogsize || errexit "max_backlog size incorrect"``  <br>
  The change is the ``.`` to an ``_`` after ``netdev``.

  Something similar has to be done in ``essdaq/config/scripts/hwcheck.sh``.
  Replace
  ```
  if [[ -f /etc/lsb-release ]]; then
    BACKLOG=net.core.netdev.max_backlog
    echo "Ubuntu: "$BACKLOG
  fi
  ```
  with
  ```
  if [[ -f /etc/lsb-release ]]; then
    BACKLOG=net.core.netdev_max_backlog
    echo "Ubuntu: "$BACKLOG
  fi
  ```
  After performing these changes, reboot!

* Wireshark might not display any network interface: make sure that wireshark is part of the sudoers group.
  To add wireshark to the sudeors group, type ``sudo adduser $USER wireshark`` in the command line.
  *Afterwards reboot*!

* When you start DAQUiri and it does not display anything and in the terminal somethin like this is written
  ```
  dateuser@MiniHP-VMM:~/VMM/essdaq/detectors/gdgem$ ./daquiri.sh
  ~/VMM/essdaq/daquiri/daquiri/build ~/VMM/essdaq/detectors/gdgem
  [2021-12-02 17:52:36.261251] [info] [processID: 9928]: <Engine> Initialized profile ""
  [2021-12-02 17:53:15.210481] [info] [processID: 9928]: <Engine> Initialized profile "NMX"
  [2021-12-02 17:53:15.210537] [info] [processID: 9928]: <ThreadRunner::run()> - action: Boot
  <Engine::boot>
  [2021-12-02 17:53:15.231560] [info] [processID: 9928]: <ESSStream:nmx_detector> booted with consumer rdkafka#consumer-1
  [2021-12-02 17:53:15.231865] [info] [processID: 9928]: <ESSStream:nmx_monitor> booted with consumer rdkafka#consumer-2
  [2021-12-02 17:53:16.232793] [info] [processID: 9928]: <ThreadRunner::run()> - action: Acquire
  [2021-12-02 17:53:16.234406] [info] [processID: 9928]: <Engine> Starting acquisition for indefinite run
  [2021-12-02 17:53:16.235760] [info] [processID: 9928]: <ESSStream:nmx_detector> Starting run, timeout: 1000
  [2021-12-02 17:53:16.235941] [info] [processID: 9928]: <ESSStream:nmx_monitor> Starting run, timeout: 1000
  [2021-12-02 17:53:16.248803] [warning] [processID: 9928]: <ESSStream> nmx_detector:0 Consume failed! Err=Subscribed topic not available: nmx_detector: Broker: Unknown topic or partition
  [2021-12-02 17:53:16.251462] [warning] [processID: 9928]: <ESSStream> nmx_monitor:0 Consume failed! Err=Subscribed topic not available: nmx_monitor: Broker: Unknown topic or partition
  [2021-12-02 17:53:21.242931] [info] [processID: 9928]:   RUNNING Elapsed: 0:00:05  Dropped spills: 0  Accepted events: 0  Dropped events: 0
  [2021-12-02 17:53:26.249695] [info] [processID: 9928]:   RUNNING Elapsed: 0:00:10  Dropped spills: 0  Accepted events: 0  Dropped events: 0
  [2021-12-02 17:53:31.256948] [info] [processID: 9928]:   RUNNING Elapsed: 0:00:15  Dropped spills: 0  Accepted events: 0  Dropped events: 0
  ```
  please make sure that the output contains really one of these statements:
  ```
  <ESSStream> nmx_monitor:0 Consume failed! Err=Subscribed topic not available: nmx_monitor: Broker: Unknown topic or partition
  ```

  If this is the case, do the following: Go to the file ``essdaq/efu/event-formation-unit/src/modules/gdgem/GdGemBase.cpp`` and replace
  in line 400 to 402 ``nmx_`` with ``NMX_``.
  The capitalisation is important!

  Then do not forget to rebuild the EFU! For this you just have to go to ``essdaq/efu/event-formation-unit/build`` and type ``make``.

* When executing the start script for the EFU in a second instance, you get the following message:
  <pre><code>
  dateuser@MiniHP-VMM:~/VMM/essdaq/detectors/gdgem$ ./start.sh
  START GDGEM
  Gd-GEM - applying local configuration
  Ubuntu: net.core.netdev_max_backlog
  Checking kernel buffer sizes
  net.core.rmem_max = 12582912
  net.core.rmem_max = 12582912
  net.core.wmem_max = 12582912
  net.core.netdev_max_backlog = 5000
  HW check PASSED
  Network check started
  Network check PASSED
  start Daquiri acquisition
  <DAQuiri::CommandServer> OK: opening new project and starting acquisition
  START EFU
  Gd-GEM - applying local configuration
  Using default essdaq location
  Extra EFU args: --file /home/dateuser/VMM/essdaq/detectors/gdgem/RHUM_config.json --calibration /home/dateuser/VMM/essdaq/detectors/gdgem/RHUM_calib.json --nohwcheck
  ~/VMM/essdaq/detectors/gdgem
  <b>
  dateuser@MiniHP-VMM:~/VMM/essdaq/detectors/gdgem$ terminate called after throwing an instance of 'std::runtime_error'
  terminate called recursively
  </b>
  </code></pre>
  It is important to note that, once the ``start.sh`` script for the EFU was executed,
  the ``stop.sh`` script has to be executed before the ``start.sh`` is executed again.

* If DAQUiri still does not show anything: make sure that the MTU of all physical network ports of the DAQ computer are set to 9000!
  If setting the MTU of one of the network ports to 9000 does not work, do the following (not recommended!): add ``--nohwcheck`` to the
  EFU start script:

  ```
  #!/bin/bash

  echo "START GDGEM"

  # change to directory of script
  cd "$(dirname "${BASH_SOURCE[0]}")" &>/dev/null
  export DETECTORDIR=$(pwd)

  source ../../config/scripts/base.sh

  systemChecks

  #
  # #
  #

  CALIBARG=""
  if [[ $EFU_CALIB != "" ]]; then
    CALIBARG="--calibration $(pwd)/${EFU_CONFIG}_calib.json"
  fi

  CONFIGARG=""
  if [[ $EFU_CONFIG != "" ]]; then
    CONFIGARG="--file $(pwd)/${EFU_CONFIG}_config.json"
  fi

  startDaquiri

  ../../efu/efu_start.sh  $CONFIGARG $CALIBARG $@ --nohwcheck
  ```

* If DAQUiri still does not show anything: go to ``essdaq/daquiri/daquiri``
  execute ``git reset --hard 9628d2eb2423a089070cd956cac5ef4666d0e0b4`` then go to the ``build`` directory
  and then execute ``cmake ..`` and ``make``.

## Hardware problems
In case there should be indeed some hardware problems, the
reader may check out this online folder, where all the documentation for
the hardware and possible fixes (also for SRS parts that are non-VMM-related)
can be found:
[Hans Muller's Google Drive](https://drive.google.com/drive/folders/1h5KLMAa7-bxbPoisnip5Adb4mYc2YMdq)

As a general rule, it is advised not to modify the hardware without consultation of Hans Muller!


## Other things
* Slow control is not working:
  - Remove firewall blocking of UDP
  - An MTU value of 9000 (jumbo frames) is needed in the network card settings
  - Check the firmware combination
* DAQ destination IP `0.0.0.0` -> HDMI not found
  - Check output of iMPACT (the log that is written into the bottom window)
  - Was the flash programmed?
* No ping:
  - Check the SFP adapter. It must be of equivalent to 1000BASE-T 1.25 GBd 3.3V
  like ABCU-5730GZ (but not ABCU-5731GZ which is the SGMII version)
  - Check the jumpers on the FEC: [FECv6 jumpers](https://drive.google.com/file/d/1MEyOzR1zJt-uHTDvpocF-teJQje0TGCv/view?usp=sharing)
  - Check the crate power on the green FEC power connector:
  [Powercrate](https://drive.google.com/file/d/1QElf_vUhjJCs6nB3YP7rbKI9PFB9p_R5/view?usp=sharing)
* _DAQuiri_ does not display data from an entire FEC (only in multi-FEC systems):
  - DO NOT try to open communication -> the ping might not work and the Slow Control falls into 'ping failed' state
  - Possible recovery: ACQ Off -> Warm Init FEC -> ACQ On
  - If Daquiri still does not show data for the FEC, restart the EFU
  - If the FEC is still missing, perform a powercycle
