Software installation
=====================

In the following a brief overview on the software installation procedure is given.
For troubleshooting, see the troubleshooting directory.

**IMPORTANT:** it should be noted that the installation order and the correct
fulfillment of the needed software requirements are important.
This means:
* Do not install the software components in a different order then the one described here.
You might miss some dependencies
* The installation procedure was only tested on Ubuntu 18.04 LTS. If you try any
other operating system or version, you might run into problems, which are not part
of this helper sheet.
* When you change your ``.bashrc`` file, do not forget to ``source`` it
or just restart your computer.

*It is recommended to read through the entire installation procedure
AND the troubleshooting part **before** actually starting the installation*

**NOTE BEFORE THE INSTALLATION:** It is NOT guaranteed that the version (a certain commit
in a certain branch) of one software component works well with the version of another
software component.
With the Slow Control, vmm-sdat and tcpdump less problems are expected.
The main possible error source for a "commit clash" could occur with the EFU
and DAQuiri.

The following commits have been tested and are known to work well together
(see in point 7 of the installation order on how to revert to certain commits). It might still require some adjustment in the code (see troubleshooting section):

* Slow Control @ e72fdf032bc206bc0d0f93d662c2a7a25ffc657a (13 Oct 2021)<br>
  ESSDAQ @ a5cfbfe5823e74eda9bf5ed82c6e113b25537763 (21 Apr 2021)<br>
  EFU @ 5abab55dcc5fa5b84abda9a8729bdc9181af0a2c (22 Sep 2021)<br>
  DAQuiri @ 9628d2eb2423a089070cd956cac5ef4666d0e0b4 (13 Aug 2021)

* Slow Control @ e72fdf032bc206bc0d0f93d662c2a7a25ffc657a (13 Oct 2021)<br>
  ESSDAQ @ 0b7b4b6a0791175ccc852dbd10ff0de1dea3d82c (02 Dec 2020)<br>
  EFU @ fdd35ce686ade098167f7c3191b6d59e6a2b79c6 (04 Jan 2021)<br>
  DAQuiri @ c2749727a14468ad43c6833f3ba3db498ee27f83 (07 Oct 2020)



**INSTALLATION ORDER**
1. Make sure that ``git`` and ``make`` are installed. If they are not
  installed, install them via
  ```
  sudo apt install git make
  ```

2. Perform the ``git clone`` commands of the software
  ```
  git clone https://gitlab.cern.ch/rd51-slow-control/vmmsc
  git clone https://github.com/ess-dmsc/essdaq
  git clone https://github.com/ess-dmsc/vmm-sdat
  ```

3. Install ``cmake`` from the Ubuntu software centre (snap package).
  The installation was successfully tested with version ``3.21.4`` of cmake.

4. Create an alias in the ``.bashrc`` for cmake:
  ```
  alias cmake='/snap/cmake/current/bin/cmake'
  ```

5. The installation of the ESS DAQ requires conan, a C++ package manager.
  Conan itself requires Python. However, with Python2.X the installation
  works not correctly. Thus, remove the Python2 packages from your system
  via
  ```
  sudo apt remove python2*
  ```
  Add then the following alias to your ``.bashrc``:
  ```
  alias python='python3'
  alias pip='pip3'
  ```

6. Install the Slow Control software, following the instructions given
  in [here](https://gitlab.cern.ch/rd51-slow-control/vmmsc/-/blob/main/README.md).
  Just as a reminder: if you run into problems during the installation,
  please check the troubleshooting at the end of this document.

7. Before installing the ESS DAQ, make sure that you have the correct version. Please revert to a certain commit via
  ```
  git reset --hard a5cfbfe5823e74eda9bf5ed82c6e113b25537763
  ```
  inside of ``/home/USERNAME/essdaq/``

8. Execute the ``install.sh`` script in the essdaq folder.
  State yes all the time and ignore the error messages.

9. Some parts of the ESS DAQ have not been build, so now they are being
  build manually. First, start with the Event Formation Unit (EFU).

  a. Go the ``essdaq/efu/event-formation-unit/build`` directory

  b. Execute ``cmake ..``

  c. You might get some errors. All these errors can be fixed. As the
  error type may vary, all common ones are listed in the troubleshooting
  part of this document.

  d. After you did the troubleshooting to the corresponding error,
  try ``cmake ..`` in the ``essdaq/efu/event-formation-unit/build`` directory.

  e. You might run into another error. Repeat this procedure, until no
  errors occur anymore.

  f. Then execute ``make``.

10. Now continue with the DAQuiri part of the ESS DAQ.
  Go to ``essdaq/daquiri/daquiri/build`` and execute ``cmake ..`` and ``make``.
  Follow the same procedure as for the EFU.

11. Now install ``ROOT``. See https://root.cern/install/ on how to install it.
  It is recommended to install the precompiled binary for Ubuntu 18.04, version ``6.24/06``: https://root.cern/releases/release-62406/.
  Do not forget to install the ROOT prequesites (already adjusted for vmm-sdat):
  ```
  sudo apt-get install dpkg-dev binutils libx11-dev libxpm-dev libxft-dev libxext-dev libssl-dev
  ```

12. Install the cluster reconstruction software ``vmm-sdat``.
  Just go in the corresponding directory, create the build-directory and
  just execute ``cmake ..`` and ``make``.

13. The installation should be successfully finished. Now continue with
  the set-up and configuration procedure.

## Configuration procedure

In the following the configuration is shown, to make the software running

* **ESS DAQ config script**:
  follow the instructions given [here](https://github.com/ess-dmsc/essdaq/blob/master/README.md) (only point 1 and 2 from there). <br>
  _If_ you need to create your own detector category, follow also point 3 from there. Otherwise use the existing **GdGEM** class.

  A tested example of the system configuration script is provided [here](https://gitlab.cern.ch/rd51-slow-control/vmm-doc/software-installation/system_GDDLAB.sh), in this folder. Please ensure to adapt the network port name (here _ens8f0_)
  and the name of the EFU calibration and EFU config files (here _GDDLAB_) to your needs.

* The _EFU calibration_ file is the one, which is generated by the calibration module of the Slow Control. Just rename it to GDDLAB_calib.json
  (replace GDDLAB with the name that you chose).

* The _EFU configuration_ file describes the detector geometry, the electronics settings and the clustering conditions for the EFU.
  It is also required for a correct display of the data in DAQuiri.

  An example is provided [here](https://gitlab.cern.ch/rd51-slow-control/vmm-doc/-/tree/master/software-installation/GDDLAB_config.json). It shows a setup with 2 FECs
  and 2 detectors with x-y-strip readout with 256 strips in each direction.
  The hybrids of the first detector are connected to the FEC 1, while the hybrids of detector 2 are connected to FEC 2.
  The hybrids on FEC 1 are connected to the DVMM ports 5, 6, 7 and 8, with 5 and 6 reading out the x-plane and 7 and 8 reading out the y-plane.
  The hybrids on FEC 2 are connected to the DVMM ports 1, 2, 6 and 7, with 1 and 2 reading out the x-plane and 6 and 7 reading out the y-plane.

  **NOTE**: to display the data in DAQuiri from the GdGEM module of the EFU, use the NMX profile of DAQuiri. It shows only one detector.
  To visualise more than one detector, increase the offset in the configuration file.
  In the given example, the coordinates of the second detector do not start in x = 0 and y = 0 but in x = 256 and y = 256.

* The network port, where you are connecting the SRS to, should have the IP address ``10.0.0.3`` with a netmask of ``255.255.255.0`` and an MTU of 9000.
